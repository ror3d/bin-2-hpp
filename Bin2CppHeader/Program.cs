﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bin2CppHeader
{
    class Program
    {
        const int MaxItemsPerLine = 15;
        static void Main(string[] args)
        {
            if (args.Length < 3)
            {
                return;
            }
            var inFileName = args[0];
            var outFileName = args[1];
            var varName = args[2];

            var regx = new System.Text.RegularExpressions.Regex("[^a-zA-Z0-9]+");
            varName = regx.Replace(varName, "_");

            var inFile = new System.IO.FileStream(inFileName, System.IO.FileMode.Open);
            var outFile = new System.IO.StreamWriter(outFileName);

            outFile.Write("const char " + varName + "[] = {\n\t");

            var linePos = 0;

            int readChar;
            bool first = true;
            while ((readChar = inFile.ReadByte()) != -1)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    if (linePos >= MaxItemsPerLine)
                    {
                        linePos = 0;
                        outFile.Write(",\n\t");
                    }
                    else
                    {
                        outFile.Write(", ");
                    }
                }
                outFile.Write("'\\x" + readChar.ToString("X2") + "'");
                linePos++;
            }
            outFile.Write("\n};\n");

            inFile.Close();
            outFile.Close();
        }
    }
}
